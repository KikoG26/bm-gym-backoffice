import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Req,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { GalleryService } from './gallery.service';

import { Request } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';

@Controller('gallery')
export class GalleryController {
  constructor(readonly galleryService: GalleryService) {}

  @Get()
  getAllImageSrcs() {
    return this.galleryService.getAllImageSrcs();
  }

  @Post()
  @UseInterceptors(FileInterceptor('imageToUpload'))
  uploadImage(
    @UploadedFile() file: Express.Multer.File,
    @Req() request: Request,
  ) {
    return this.galleryService.uploadImage(file);
  }

  @Delete(':fileName')
  deleteImage(@Param('fileName') fileName: string) {
    return this.galleryService.deleteImage(fileName);
  }
}
