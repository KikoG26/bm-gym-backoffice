import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class GalleryService {
  private staticFolder = path.join(process.cwd(), '/static');
  getAllImageSrcs(): string[] {
    let fileArray: string[] = [];
    fileArray = fs.readdirSync(this.staticFolder);
    return fileArray;
  }

  async uploadImage(file) {
    if (!file) {
      throw new HttpException(
        'Nebol nahraný žiadny súbor',
        HttpStatus.NOT_FOUND,
      );
    }
    try {
      const fileToUpload = file;
      const filePath = path.join(this.staticFolder, fileToUpload.originalname);
      fs.writeFileSync(filePath, fileToUpload.buffer);
      return fileToUpload;
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  async deleteImage(fileName: string) {
    const fileToDelete = await path.join(this.staticFolder, fileName);
    if (!fileToDelete) {
      throw new HttpException(fileToDelete, HttpStatus.NOT_FOUND);
    }
    fs.rm(fileToDelete, () => console.log('file deleted'));
    return HttpStatus.OK;
  }
}
