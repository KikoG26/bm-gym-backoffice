import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Request } from 'express';

import { PrismaService } from 'src/prisma.service';
import { USERS } from '@prisma/client';
import {
  verificationMail,
  sendPinMail,
  sendForgottenPinMail,
} from 'src/utils/email/email';
import { decodeHash } from 'src/utils/hash';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async findAll(): Promise<USERS[]> {
    const users = await this.prisma.uSERS.findMany();
    if (!users) {
      throw new HttpException(
        'Používatelov sa nepodarilo vyhľadať',
        HttpStatus.NOT_FOUND,
      );
    }
    return users;
  }

  async findOne(pin: number) {
    try {
      const user = await this.prisma.uSERS.findFirst({
        where: {
          USR_PIN: pin,
        },
      });
      if (!user) {
        throw new NotFoundException('Zadali ste nesprávny PIN');
      }
      return {
        status: 'OK',
        data: user,
      };
    } catch (e) {
      return {
        status: e.response.statusCode,
        message: e.response.message,
      };
    }
  }

  //Registration part 1

  async verify(email: string) {
    if (!email) {
      throw new BadRequestException('Nie sú vyplnené všetky údaje.');
    }

    const mailRegex: RegExp = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      'gm',
    );

    const isValidEmail = mailRegex.test(email);

    if (!isValidEmail) {
      throw new BadRequestException('Nesprávny formát e-mailu');
    }
    const alreadyExists = await this.prisma.uSERS.findFirst({
      where: { USR_EMAIL: email },
    });
    if (alreadyExists) {
      throw new BadRequestException('Používateľ s daným e-mailom už existuje');
    }

    verificationMail(email);
    return {
      status: 'Success',
      message: 'Email sent.',
    };
  }

  //Po kliku na btn pouzivatela presmeruje na FE stranku s potvrdenim, ktora na onMount spravi Post request na spodnu routu
  //Na FE bude mat routa tvar domain/rezervace/potvrzeni/hash -> hash preto, aby sa mohol ulozit z params a poslat na backend create routu
  async create(hash: string): Promise<USERS> {
    const { email } = decodeHash(hash);
    if (!email) {
      throw new NotFoundException();
    }
    const pin = await this.generatePin();

    const newUser = await this.prisma.uSERS.create({
      data: {
        USR_EMAIL: email,
        USR_PIN: pin,
      },
    });

    if (!newUser) {
      throw new InternalServerErrorException(
        'Nepodarilo sa vytvoriť nového člena.',
      );
    }
    sendPinMail(email, pin);
    return newUser;
  }

  async handleForgottenPin(email: string): Promise<USERS> {
    if (!email) {
      throw new BadRequestException('Prosím vyplňte e-mail.');
    }
    const user = await this.prisma.uSERS.findFirst({
      where: { USR_EMAIL: email },
    });

    if (!user) {
      throw new NotFoundException(
        'K zadanej e-mailovej adrese neexistuje vytvorený účet.',
      );
    }
    const { USR_PIN: pin } = user;
    sendForgottenPinMail(email, pin);
    return user;
  }

  async generatePin(): Promise<number> {
    let generatePin = true;
    while (generatePin) {
      let pin = Math.floor(1000 + Math.random() * 9000);
      let selectedPin = await this.prisma.uSERS.findFirst({
        where: { USR_PIN: pin },
      });

      if (!selectedPin) {
        generatePin = false;
        return pin;
      }
    }
  }
}
