import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { Request } from 'express';
import { Public } from 'src/auth/decorators/public.decorator';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @Public()
  @Get(':pin')
  findOne(@Param('pin') pin: string) {
    return this.userService.findOne(parseInt(pin));
  }

  @Public()
  @Post()
  verify(@Req() request: Request) {
    return this.userService.verify(request.body.email);
  }

  @Public()
  @Post('skuska/forgotten-pin')
  handleForgottenPin(@Req() request: Request) {
    return this.userService.handleForgottenPin(request.body.email);
  }

  @Public()
  @Post(':hash')
  create(@Param('hash') hash: string) {
    return this.userService.create(hash);
  }
}
