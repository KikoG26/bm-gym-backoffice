import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { STATUS_CODES } from 'http';
import { PrismaService } from 'src/prisma.service';
import { TrainingService } from 'src/training/training.service';
import { UserService } from 'src/user/user.service';
import {
  sendReservationMail,
  sendChangeReservationMail,
} from 'src/utils/email/email';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ReservationService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly userService: UserService,
    private readonly trainingService: TrainingService,
  ) {}

  async createReservation(nameArr: string[], pin: number, trainingId: number) {
    try {
      if (nameArr.length <= 0 || !pin || !trainingId) {
        throw new BadRequestException('Nie su zadane vsetky nutne udaje');
      }

      const user = await this.userService.findOne(pin);
      if (!user || user.status === 404) {
        throw new NotFoundException('Nesprávny PIN');
      }

      const training = await this.trainingService.getOne(trainingId.toString());
      if (!training) {
        throw new NotFoundException('Neexistujúci tréning');
      }

      let allFreePlaces =
        training.TN_MAXMEM + training.TN_MAXSUB - training.RESERVATION.length;

      if (nameArr.length > allFreePlaces) {
        throw new BadRequestException('Nedostatok volneho miesta');
      }

      const userEmail = user.data.USR_EMAIL;
      const publicId = uuidv4();

      const data = nameArr.map((name) => {
        return {
          RES_TN_ID: training.TN_ID,
          RES_PUBLIC_ID: publicId,
          RES_USR_EMAIL: userEmail,
          RES_FULLNAME: name,
          RES_IS_SUB: allFreePlaces > training.TN_MAXSUB ? 0 : 1,
        };
      });

      const createdReservation = await this.prisma.rESERVATION.createMany({
        data: data,
      });

      if (!createdReservation) {
        throw new BadRequestException('Chyba pri vytvarani rezervacie.');
      }
      sendReservationMail(userEmail, data[0].RES_PUBLIC_ID);
      return {
        status: 'OK',
        data: createdReservation,
      };
    } catch (e) {
      if (e.status === 400) {
        throw new BadRequestException(e.message);
      }
      if (e.status === 404) {
        throw new NotFoundException(e.message);
      }
    }
  }

  async toggleIsSub(id: number) {
    try {
      const reservation = await this.prisma.rESERVATION.findFirst({
        where: {
          RES_ID: id,
        },
      });
      if (!reservation) {
        throw new NotFoundException(
          'Neexistujuca rezervácia',
          STATUS_CODES[HttpStatus.NOT_FOUND],
        );
      }

      const changedReservation = await this.prisma.rESERVATION.update({
        where: {
          RES_ID: reservation.RES_ID,
        },
        data: {
          RES_IS_SUB: reservation.RES_IS_SUB === 1 ? 0 : 1,
        },
      });
      sendChangeReservationMail(
        changedReservation.RES_USR_EMAIL,
        changedReservation.RES_PUBLIC_ID,
      );
      return {
        message: 'Úspešne prepnutý status náhrady',
        status: STATUS_CODES[HttpStatus.OK],
        changedData: changedReservation,
        oldData: reservation,
      };
    } catch (e) {
      if (e instanceof Error) {
        return {
          message: e.message,
          type: 'Error',
        };
      }
    }
  }

  async toggleIsRemoved(id: number) {
    try {
      const reservation = await this.prisma.rESERVATION.findFirst({
        where: {
          RES_ID: id,
        },
      });
      if (!reservation) {
        throw new NotFoundException(
          'Neexistujuca rezervácia',
          STATUS_CODES[HttpStatus.NOT_FOUND],
        );
      }

      const changedReservation = await this.prisma.rESERVATION.update({
        where: {
          RES_ID: reservation.RES_ID,
        },
        data: {
          RES_REMOVED: reservation.RES_REMOVED === 1 ? 0 : 1,
        },
      });

      sendChangeReservationMail(
        changedReservation.RES_USR_EMAIL,
        changedReservation.RES_PUBLIC_ID,
      );

      return {
        message: 'Úspešne prepnutý status vymazania',
        status: STATUS_CODES[HttpStatus.OK],
        changedData: changedReservation,
        oldData: reservation,
      };
    } catch (e) {
      if (e instanceof Error) {
        return {
          message: e.message,
          type: 'Error',
        };
      }
    }
  }
}
