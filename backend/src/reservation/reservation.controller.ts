import { Controller, Get, Param, Put, Req, Post } from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { Public } from 'src/auth/decorators/public.decorator';

import { Request } from 'express';

@Controller('reservation')
export class ReservationController {
  constructor(private readonly reservationService: ReservationService) {}

  @Public()
  @Post()
  async createReservation(@Req() request: Request) {
    return await this.reservationService.createReservation(
      request.body.nameArr,
      request.body.pin,
      request.body.trainingId,
    );
  }

  @Put(':id')
  async toggleIsSub(@Param('id') id: string) {
    return this.reservationService.toggleIsSub(parseInt(id));
  }

  @Put('/remove/:id')
  async toggleIsRemoved(@Param('id') id: string) {
    return this.reservationService.toggleIsRemoved(parseInt(id));
  }
}
