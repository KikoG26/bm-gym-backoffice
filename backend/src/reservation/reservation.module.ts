import { Module } from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { ReservationController } from './reservation.controller';
import { PrismaModule } from 'src/prisma.module';
import { UserModule } from 'src/user/user.module';
import { TrainingModule } from 'src/training/training.module';

@Module({
  imports: [PrismaModule, UserModule, TrainingModule],
  providers: [ReservationService],
  controllers: [ReservationController],
})
export class ReservationModule {}
