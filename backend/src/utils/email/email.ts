import * as nodemailer from 'nodemailer';
import * as path from 'path';
import * as fs from 'fs';

import { createHash } from '../hash';

enum Status {
  SUCCESS,
  FAIL,
}

interface IEmailResponse {
  status: Status;
}

const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  secure: true,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASSWORD,
  },
});

const placehold = (template, data) => {
  if (typeof template !== 'string') {
    throw new TypeError(
      `Expected a \`string\` in the first argument, got \`${typeof template}\``,
    );
  }

  if (typeof data !== 'object') {
    throw new TypeError(
      `Expected an \`object\` or \`Array\` in the second argument, got \`${typeof data}\``,
    );
  }

  const hashRegex = /##(\d+|[a-z$_][a-z\d$_]*?(?:\.[a-z\d$_]*?)*?)##/gi;

  return template.replace(hashRegex, (_, key) => {
    let result = data;

    for (const property of key.split('.')) {
      result = result ? result[property] : '';
    }

    return String(result);
  });
};

export const verificationMail = (email: string) => {
  const template = fs.readFileSync(
    path.resolve('src/utils/email/templates/', 'verificationMail.html'),
    { encoding: 'utf-8' },
  );
  const link = `http://${
    process.env.PROJECT_ADDRESS
  }/registrace/potvrzeni/${createHash(email)}`;
  const mailContent = {
    text: `Dobrý den, děkujeme za registraci. Pro pokračování v registraci prosím kliknětě na odkaz uvedený níže. ${link}.
    Neodpovídejte prosím na tento E-Mail, byl vygenerován automaticky.`,
    html: placehold(template, {
      LINK: link,
      PROJNAME: process.env.PROJECT_NAME,
    }),
  };
  var message = {
    from: `${process.env.PROJECT_NAME} <${process.env.SMTP_USER}>`,
    to: email,
    subject: 'Potvrzení E-mailové adresy',
    text: mailContent.text,
    html: mailContent.html,
  };
  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return true;
    }
  });
};

export const sendPinMail = (email: string, pin: number) => {
  const template = fs.readFileSync(
    path.resolve('./src/utils/email/templates', 'pinMail.html'),
    { encoding: 'utf-8' },
  );

  const mailContent = {
    text: `Váš přihlašovací PIN je: ${pin}. Neodpovídejte prosím na tento E-Mail, byl vygenerován automaticky.`,
    html: placehold(template, { PIN: pin, PROJNAME: process.env.PROJECT_NAME }),
  };

  var message = {
    from: `${process.env.PROJECT_NAME} <${process.env.SMTP_USER}>`,
    to: email,
    subject: 'Přihlašovací PIN',
    text: mailContent.text,
    html: mailContent.html,
  };

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return true;
    }
  });
};

export const sendForgottenPinMail = (email: string, pin: number): void => {
  const template = fs.readFileSync(
    path.resolve('./src/utils/email/templates', 'forgottenMail.html'),
    { encoding: 'utf-8' },
  );

  const mailContent = {
    text: `Váš přihlašovací PIN je: ${pin}. Neodpovídejte prosím na tento E-Mail, byl vygenerován automaticky.`,
    html: placehold(template, { PIN: pin, PROJNAME: process.env.PROJECT_NAME }),
  };

  var message = {
    from: `${process.env.PROJECT_NAME} <${process.env.SMTP_USER}>`,
    to: email,
    subject: 'Přihlašovací PIN',
    text: mailContent.text,
    html: mailContent.html,
  };

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return true;
    }
  });
};

export const sendReservationMail = (email, publicId) => {
  const template = fs.readFileSync(
    path.resolve('src/utils/email/templates/', 'reservationMail.html'),
    { encoding: 'utf-8' },
  );
  const link = `http://${process.env.PROJECT_ADDRESS}/rezervace/prehled/${publicId}`;

  const mailContent = {
    text: `Děkujeme za rezervaci. Váš odkaz pro tuto rezervaci je: ${link}. Neodpovídejte prosím na tento E-Mail, byl vygenerován automaticky.`,
    html: placehold(template, {
      LINK: link,
      PROJNAME: process.env.PROJECT_NAME,
    }),
  };

  var message = {
    from: `${process.env.PROJECT_NAME} <${process.env.SMTP_USER}>`,
    to: email,
    subject: 'Detail rezervace',
    text: mailContent.text,
    html: mailContent.html,
  };

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return true;
    }
  });
};

export const sendChangeReservationMail = (email, publicId) => {
  const template = fs.readFileSync(
    path.resolve('src/utils/email/templates/', 'reservationTypeMail.html'),
    { encoding: 'utf-8' },
  );

  const link = `http://${process.env.PROJECT_ADDRESS}/rezervace/prehled/${publicId}`;

  const mailContent = {
    text: `Dobrý den, rádi bychom Vás informovali, že došlo ke změně stavu nad Vaší rezervací.
    Prohlédněte si změny zde: ${link}`,
    html: placehold(template, {
      LINK: link,
      PROJNAME: process.env.PROJECT_NAME,
    }),
  };

  var message = {
    from: `${process.env.PROJECT_NAME} <${process.env.SMTP_USER}>`,
    to: email,
    subject: 'Změna stavu rezervace',
    text: mailContent.text,
    html: mailContent.html,
  };

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return true;
    }
  });
  return true;
};
