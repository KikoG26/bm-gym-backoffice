import { JwtService } from '@nestjs/jwt';

const jwtService = new JwtService();

export const createHash = (email) => {
  const token = jwtService.sign(
    { email },
    { secret: process.env.JWT_SECRET, expiresIn: '1h' },
  );
  return token;
};

export const decodeHash = (hash) => {
  try {
    const email = jwtService.verify(hash, { secret: process.env.JWT_SECRET });
    return email;
  } catch (e) {
    throw new Error(e);
  }
};
