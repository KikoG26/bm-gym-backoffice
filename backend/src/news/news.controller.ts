import { Controller, Get, Param, Put, Req, Post, Delete } from '@nestjs/common';
import { NewsService } from './news.service';
import { Request } from 'express';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get()
  getAll() {
    return this.newsService.getAll();
  }

  @Put(':id')
  updateOne(@Param('id') id: string, @Req() request: Request) {
    return this.newsService.updateOne(
      id,
      request.body.text,
      request.body.heading,
    );
  }

  @Post()
  add(@Req() request: Request) {
    return this.newsService.add(request.body.text, request.body.heading);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.newsService.delete(id);
  }
}
