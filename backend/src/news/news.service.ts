import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class NewsService {
  constructor(private readonly prisma: PrismaService) {}

  async getAll() {
    const news = await this.prisma.aCTUALITY.findMany();
    if (!news) {
      return new NotFoundException('Žádná aktualita.');
    }
    return news;
  }

  async updateOne(id: string, text?: string, heading?: string) {
    if (!text && !heading) {
      throw new BadRequestException('Nebola zadaná žiadna hodnota.');
    }
    const newsToUpdate = await this.prisma.aCTUALITY.update({
      where: {
        ACT_ID: parseInt(id),
      },
      data: {
        ACT_TEXT: text || undefined,
        ACT_HEADING: heading || undefined,
      },
    });
    if (!newsToUpdate) {
      throw new BadRequestException('Pri úprave nastala chyba.');
    } else {
      return newsToUpdate;
    }
  }

  async add(text: string, heading: string) {
    if (!text || !heading) {
      throw new BadRequestException('Prosím vyplňte všetky potrebné políčka.');
    }
    const newActuality = await this.prisma.aCTUALITY.create({
      data: {
        ACT_HEADING: heading,
        ACT_TEXT: text,
      },
    });
    if (!newActuality) {
      throw new InternalServerErrorException(
        'Chyba pri pridávaní novej aktuality.',
      );
    }
    return newActuality;
  }

  async delete(id: string) {
    const actualityToDelete = await this.prisma.aCTUALITY.delete({
      where: {
        ACT_ID: parseInt(id),
      },
    });
    if (!actualityToDelete) {
      throw new InternalServerErrorException(
        'Nepodarilo sa vymazať aktualitu.',
      );
    }
    return actualityToDelete;
  }
}
