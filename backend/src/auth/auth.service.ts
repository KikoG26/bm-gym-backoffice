import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { PrismaService } from 'src/prisma.service';

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService, private jwtService: JwtService) {}

  async signIn(username: string, password: string) {
    if (!username || !password) {
      throw new BadRequestException('Jméno, nebo heslo není vyplněno');
    }
    const user = await this.prisma.aDMIN.findUnique({
      where: { ADM_LOGIN: username },
    });
    if (!user) {
      throw new NotFoundException('Uživatel nebyl nalezen');
    }
    if (user.ADM_PASSWORD !== password) {
      throw new UnauthorizedException('Špatné heslo');
    }

    const payload = { username: user.ADM_LOGIN, sub: user.ADM_PASSWORD };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
