import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { NewsModule } from './news/news.module';
import { TrainingModule } from './training/training.module';
import { GalleryModule } from './gallery/gallery.module';

import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ReservationModule } from './reservation/reservation.module';

@Module({
  imports: [
    UserModule,
    AuthModule,
    NewsModule,
    TrainingModule,
    GalleryModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'static'),
    }),
    ReservationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
