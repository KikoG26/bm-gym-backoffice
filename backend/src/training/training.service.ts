import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Prisma, TRAININGS } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';

import * as moment from 'moment';

@Injectable()
export class TrainingService {
  constructor(private readonly prisma: PrismaService) {}

  async getAll(
    page?: number,
    name?: string,
    hour?: string,
    dateFrom?: string,
    dateTo?: string,
    showAll?: boolean,
  ) {
    const trainings = await this.prisma.tRAININGS.findMany({
      take: !showAll ? 50 : undefined,
      skip: !page || page == 1 ? 0 : (page - 1) * 50,
      include: {
        RESERVATION: true,
        _count: {
          select: {
            RESERVATION: true,
          },
        },
      },
      orderBy: {
        TN_DATE: 'desc',
      },
      where: {
        TN_NAME: {
          contains: name,
        },
        TN_HOUR: {
          contains: hour,
        },
        TN_DATE: {
          gte: dateFrom ? moment(dateFrom).toDate() : undefined,
          lte: dateTo ? moment(dateTo).toDate() : undefined,
        },
      },
    });

    if (!trainings) {
      throw new NotFoundException('Nebyli nalezené žádne tréningy');
    }
    return trainings;
  }

  async getOne(id: string) {
    const training = await this.prisma.tRAININGS.findFirst({
      where: { TN_ID: parseInt(id) },
      include: {
        RESERVATION: true,
      },
    });
    if (!training) {
      throw new NotFoundException('Neexistuje tréning s daným ID.');
    }
    return training;
  }

  async createTraining(
    cycle: number,
    date: Date,
    name: string,
    hour: string,
    maxMembers: number,
    maxSubs: number,
    description?: string,
    color?: string,
    bgColor?: string,
  ) {
    if (!cycle || !date || !name || !hour || !maxMembers || !maxSubs) {
      throw new BadRequestException('Prosím vyplňte všetky potrebné políčka');
    }
    if (cycle === 1) {
      const training = await this.prisma.tRAININGS.create({
        data: {
          TN_NAME: name,
          TN_DATE: new Date(date),
          TN_HOUR: hour,
          TN_MAXMEM: maxMembers,
          TN_MAXSUB: maxSubs,
          TN_REMARK: description || undefined,
          TN_COLOR: color || undefined,
          TN_BGCOLOR: bgColor || undefined,
        },
      });
      if (!training) {
        throw new InternalServerErrorException(
          'Nastala chyba pri vytvorení tréningu.',
        );
      }
      return training;
    } else if (cycle > 1) {
      const allTrainings = [];
      for (let i = 0; i < cycle; i++) {
        const training = {
          TN_NAME: name,
          TN_DATE: new Date(
            new Date(date).setDate(new Date(date).getDate() + 7 * i),
          ),
          TN_HOUR: hour,
          TN_MAXMEM: maxMembers,
          TN_MAXSUB: maxSubs,
          TN_REMARK: description || undefined,
          TN_COLOR: color || undefined,
          TN_BGCOLOR: bgColor || undefined,
        };
        allTrainings.push(training);
      }

      const trainings = await this.prisma.tRAININGS.createMany({
        data: allTrainings,
      });
      if (!trainings) {
        throw new InternalServerErrorException(
          'Nastala chyba pri vytvorení tréningu.',
        );
      }
      return trainings;
    }
  }

  async update(
    id: string,
    date?: Date,
    name?: string,
    hour?: string,
    maxMembers?: number,
    maxSubs?: number,
    description?: string,
    color?: string,
    bgColor?: string,
  ) {
    if (
      !date &&
      !name &&
      !hour &&
      !maxMembers &&
      !maxSubs &&
      !description &&
      !color &&
      bgColor
    ) {
      throw new BadRequestException('Prosím vyplňte potrebné údaje');
    }
    const editedTraining = await this.prisma.tRAININGS.update({
      where: {
        TN_ID: parseInt(id),
      },
      data: {
        TN_DATE: date ? new Date(date) : undefined,
        TN_NAME: name || undefined,
        TN_BGCOLOR: bgColor || undefined,
        TN_COLOR: color || undefined,
        TN_HOUR: hour || undefined,
        TN_MAXMEM: maxMembers || undefined,
        TN_MAXSUB: maxSubs || undefined,
        TN_REMARK: description || undefined,
      },
    });
    if (!editedTraining) {
      throw new NotFoundException('Neexistuje upravovaný tréning');
    }
    return editedTraining;
  }

  async delete(id: string) {
    const deletedPost = await this.prisma.tRAININGS.delete({
      where: {
        TN_ID: parseInt(id),
      },
    });
    if (!deletedPost) {
      throw new NotFoundException('Stala sa chyba pri mazání tréningu');
    }
    return deletedPost;
  }
}
