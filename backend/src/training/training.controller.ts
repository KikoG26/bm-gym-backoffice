import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { TrainingService } from './training.service';
import { Request } from 'express';

@Controller('training')
export class TrainingController {
  constructor(private readonly trainingService: TrainingService) {}

  @Get()
  getAll(
    @Query('page') page: string,
    @Query('name') name: string,
    @Query('hour') hour: string,
    @Query('dateFrom') dateFrom: string,
    @Query('dateTo') dateTo: string,
    @Query('showAll') showAll: boolean,
  ) {
    return this.trainingService.getAll(
      parseInt(page),
      name,
      hour,
      dateFrom,
      dateTo,
      showAll,
    );
  }

  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.trainingService.getOne(id);
  }

  @Post()
  create(@Req() request: Request) {
    return this.trainingService.createTraining(
      request.body.cycle,
      request.body.date,
      request.body.name,
      request.body.hour,
      request.body.maxMembers,
      request.body.maxSubs,
      request.body.description,
      request.body.color,
      request.body.bgColor,
    );
  }

  @Put(':id')
  update(@Param('id') id: string, @Req() request: Request) {
    return this.trainingService.update(
      id,
      request.body.date,
      request.body.name,
      request.body.hour,
      request.body.maxMembers,
      request.body.maxSubs,
      request.body.description,
      request.body.color,
      request.body.bgColor,
    );
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.trainingService.delete(id);
  }
}
