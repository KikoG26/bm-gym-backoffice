import { NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
export declare class NewsService {
    private readonly prisma;
    constructor(prisma: PrismaService);
    getAll(): Promise<NotFoundException | import(".prisma/client").ACTUALITY[]>;
    updateOne(id: string, text?: string, heading?: string): Promise<import(".prisma/client").ACTUALITY>;
    add(text: string, heading: string): Promise<import(".prisma/client").ACTUALITY>;
    delete(id: string): Promise<import(".prisma/client").ACTUALITY>;
}
