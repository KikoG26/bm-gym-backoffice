import { NewsService } from './news.service';
import { Request } from 'express';
export declare class NewsController {
    private readonly newsService;
    constructor(newsService: NewsService);
    getAll(): Promise<import("@nestjs/common").NotFoundException | import(".prisma/client").ACTUALITY[]>;
    updateOne(id: string, request: Request): Promise<import(".prisma/client").ACTUALITY>;
    add(request: Request): Promise<import(".prisma/client").ACTUALITY>;
    delete(id: string): Promise<import(".prisma/client").ACTUALITY>;
}
