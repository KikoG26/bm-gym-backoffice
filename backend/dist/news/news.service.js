"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewsService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma.service");
let NewsService = class NewsService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async getAll() {
        const news = await this.prisma.aCTUALITY.findMany();
        if (!news) {
            return new common_1.NotFoundException('Žádná aktualita.');
        }
        return news;
    }
    async updateOne(id, text, heading) {
        if (!text && !heading) {
            throw new common_1.BadRequestException('Nebola zadaná žiadna hodnota.');
        }
        const newsToUpdate = await this.prisma.aCTUALITY.update({
            where: {
                ACT_ID: parseInt(id),
            },
            data: {
                ACT_TEXT: text || undefined,
                ACT_HEADING: heading || undefined,
            },
        });
        if (!newsToUpdate) {
            throw new common_1.BadRequestException('Pri úprave nastala chyba.');
        }
        else {
            return newsToUpdate;
        }
    }
    async add(text, heading) {
        if (!text || !heading) {
            throw new common_1.BadRequestException('Prosím vyplňte všetky potrebné políčka.');
        }
        const newActuality = await this.prisma.aCTUALITY.create({
            data: {
                ACT_HEADING: heading,
                ACT_TEXT: text,
            },
        });
        if (!newActuality) {
            throw new common_1.InternalServerErrorException('Chyba pri pridávaní novej aktuality.');
        }
        return newActuality;
    }
    async delete(id) {
        const actualityToDelete = await this.prisma.aCTUALITY.delete({
            where: {
                ACT_ID: parseInt(id),
            },
        });
        if (!actualityToDelete) {
            throw new common_1.InternalServerErrorException('Nepodarilo sa vymazať aktualitu.');
        }
        return actualityToDelete;
    }
};
NewsService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], NewsService);
exports.NewsService = NewsService;
//# sourceMappingURL=news.service.js.map