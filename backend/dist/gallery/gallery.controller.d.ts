/// <reference types="multer" />
import { GalleryService } from './gallery.service';
import { Request } from 'express';
export declare class GalleryController {
    readonly galleryService: GalleryService;
    constructor(galleryService: GalleryService);
    getAllImageSrcs(): string[];
    uploadImage(file: Express.Multer.File, request: Request): Promise<any>;
    deleteImage(fileName: string): Promise<import("@nestjs/common").HttpStatus>;
}
