import { HttpStatus } from '@nestjs/common';
export declare class GalleryService {
    private staticFolder;
    getAllImageSrcs(): string[];
    uploadImage(file: any): Promise<any>;
    deleteImage(fileName: string): Promise<HttpStatus>;
}
