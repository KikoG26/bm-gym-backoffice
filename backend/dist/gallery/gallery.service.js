"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GalleryService = void 0;
const common_1 = require("@nestjs/common");
const fs = require("fs");
const path = require("path");
let GalleryService = class GalleryService {
    constructor() {
        this.staticFolder = path.join(process.cwd(), '/static');
    }
    getAllImageSrcs() {
        let fileArray = [];
        fileArray = fs.readdirSync(this.staticFolder);
        return fileArray;
    }
    async uploadImage(file) {
        if (!file) {
            throw new common_1.HttpException('Nebol nahraný žiadny súbor', common_1.HttpStatus.NOT_FOUND);
        }
        try {
            const fileToUpload = file;
            const filePath = path.join(this.staticFolder, fileToUpload.originalname);
            fs.writeFileSync(filePath, fileToUpload.buffer);
            return fileToUpload;
        }
        catch (e) {
            throw new common_1.HttpException(e.message, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async deleteImage(fileName) {
        const fileToDelete = await path.join(this.staticFolder, fileName);
        if (!fileToDelete) {
            throw new common_1.HttpException(fileToDelete, common_1.HttpStatus.NOT_FOUND);
        }
        fs.rm(fileToDelete, () => console.log('file deleted'));
        return common_1.HttpStatus.OK;
    }
};
GalleryService = __decorate([
    (0, common_1.Injectable)()
], GalleryService);
exports.GalleryService = GalleryService;
//# sourceMappingURL=gallery.service.js.map