import { UserService } from './user.service';
import { Request } from 'express';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    findAll(): Promise<import(".prisma/client").USERS[]>;
    findOne(pin: string): Promise<{
        status: string;
        data: import(".prisma/client").USERS;
        message?: undefined;
    } | {
        status: any;
        message: any;
        data?: undefined;
    }>;
    verify(request: Request): Promise<{
        status: string;
        message: string;
    }>;
    handleForgottenPin(request: Request): Promise<import(".prisma/client").USERS>;
    create(hash: string): Promise<import(".prisma/client").USERS>;
}
