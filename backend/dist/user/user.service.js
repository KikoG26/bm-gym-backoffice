"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma.service");
const email_1 = require("../utils/email/email");
const hash_1 = require("../utils/hash");
let UserService = class UserService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAll() {
        const users = await this.prisma.uSERS.findMany();
        if (!users) {
            throw new common_1.HttpException('Používatelov sa nepodarilo vyhľadať', common_1.HttpStatus.NOT_FOUND);
        }
        return users;
    }
    async findOne(pin) {
        try {
            const user = await this.prisma.uSERS.findFirst({
                where: {
                    USR_PIN: pin,
                },
            });
            if (!user) {
                throw new common_1.NotFoundException('Zadali ste nesprávny PIN');
            }
            return {
                status: 'OK',
                data: user,
            };
        }
        catch (e) {
            return {
                status: e.response.statusCode,
                message: e.response.message,
            };
        }
    }
    async verify(email) {
        if (!email) {
            throw new common_1.BadRequestException('Nie sú vyplnené všetky údaje.');
        }
        const mailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'gm');
        const isValidEmail = mailRegex.test(email);
        if (!isValidEmail) {
            throw new common_1.BadRequestException('Nesprávny formát e-mailu');
        }
        const alreadyExists = await this.prisma.uSERS.findFirst({
            where: { USR_EMAIL: email },
        });
        if (alreadyExists) {
            throw new common_1.BadRequestException('Používateľ s daným e-mailom už existuje');
        }
        (0, email_1.verificationMail)(email);
        return {
            status: 'Success',
            message: 'Email sent.',
        };
    }
    async create(hash) {
        const { email } = (0, hash_1.decodeHash)(hash);
        if (!email) {
            throw new common_1.NotFoundException();
        }
        const pin = await this.generatePin();
        const newUser = await this.prisma.uSERS.create({
            data: {
                USR_EMAIL: email,
                USR_PIN: pin,
            },
        });
        if (!newUser) {
            throw new common_1.InternalServerErrorException('Nepodarilo sa vytvoriť nového člena.');
        }
        (0, email_1.sendPinMail)(email, pin);
        return newUser;
    }
    async handleForgottenPin(email) {
        if (!email) {
            throw new common_1.BadRequestException('Prosím vyplňte e-mail.');
        }
        const user = await this.prisma.uSERS.findFirst({
            where: { USR_EMAIL: email },
        });
        if (!user) {
            throw new common_1.NotFoundException('K zadanej e-mailovej adrese neexistuje vytvorený účet.');
        }
        const { USR_PIN: pin } = user;
        (0, email_1.sendForgottenPinMail)(email, pin);
        return user;
    }
    async generatePin() {
        let generatePin = true;
        while (generatePin) {
            let pin = Math.floor(1000 + Math.random() * 9000);
            let selectedPin = await this.prisma.uSERS.findFirst({
                where: { USR_PIN: pin },
            });
            if (!selectedPin) {
                generatePin = false;
                return pin;
            }
        }
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map