import { PrismaService } from 'src/prisma.service';
import { USERS } from '@prisma/client';
export declare class UserService {
    private prisma;
    constructor(prisma: PrismaService);
    findAll(): Promise<USERS[]>;
    findOne(pin: number): Promise<{
        status: string;
        data: USERS;
        message?: undefined;
    } | {
        status: any;
        message: any;
        data?: undefined;
    }>;
    verify(email: string): Promise<{
        status: string;
        message: string;
    }>;
    create(hash: string): Promise<USERS>;
    handleForgottenPin(email: string): Promise<USERS>;
    generatePin(): Promise<number>;
}
