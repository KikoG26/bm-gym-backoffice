"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodeHash = exports.createHash = void 0;
const jwt_1 = require("@nestjs/jwt");
const jwtService = new jwt_1.JwtService();
const createHash = (email) => {
    const token = jwtService.sign({ email }, { secret: process.env.JWT_SECRET, expiresIn: '1h' });
    return token;
};
exports.createHash = createHash;
const decodeHash = (hash) => {
    try {
        const email = jwtService.verify(hash, { secret: process.env.JWT_SECRET });
        return email;
    }
    catch (e) {
        throw new Error(e);
    }
};
exports.decodeHash = decodeHash;
//# sourceMappingURL=hash.js.map