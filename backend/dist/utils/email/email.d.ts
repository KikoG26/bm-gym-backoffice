export declare const verificationMail: (email: string) => void;
export declare const sendPinMail: (email: string, pin: number) => void;
export declare const sendForgottenPinMail: (email: string, pin: number) => void;
export declare const sendReservationMail: (email: any, publicId: any) => void;
export declare const sendChangeReservationMail: (email: any, publicId: any) => boolean;
