import { Prisma, TRAININGS } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';
export declare class TrainingService {
    private readonly prisma;
    constructor(prisma: PrismaService);
    getAll(page?: number, name?: string, hour?: string, dateFrom?: string, dateTo?: string, showAll?: boolean): Promise<(TRAININGS & {
        RESERVATION: import(".prisma/client").RESERVATION[];
        _count: {
            RESERVATION: number;
        };
    })[]>;
    getOne(id: string): Promise<TRAININGS & {
        RESERVATION: import(".prisma/client").RESERVATION[];
    }>;
    createTraining(cycle: number, date: Date, name: string, hour: string, maxMembers: number, maxSubs: number, description?: string, color?: string, bgColor?: string): Promise<TRAININGS | Prisma.BatchPayload>;
    update(id: string, date?: Date, name?: string, hour?: string, maxMembers?: number, maxSubs?: number, description?: string, color?: string, bgColor?: string): Promise<TRAININGS>;
    delete(id: string): Promise<TRAININGS>;
}
