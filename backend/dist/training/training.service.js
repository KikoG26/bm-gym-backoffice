"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrainingService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma.service");
const moment = require("moment");
let TrainingService = class TrainingService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async getAll(page, name, hour, dateFrom, dateTo, showAll) {
        const trainings = await this.prisma.tRAININGS.findMany({
            take: !showAll ? 50 : undefined,
            skip: !page || page == 1 ? 0 : (page - 1) * 50,
            include: {
                RESERVATION: true,
                _count: {
                    select: {
                        RESERVATION: true,
                    },
                },
            },
            orderBy: {
                TN_DATE: 'desc',
            },
            where: {
                TN_NAME: {
                    contains: name,
                },
                TN_HOUR: {
                    contains: hour,
                },
                TN_DATE: {
                    gte: dateFrom ? moment(dateFrom).toDate() : undefined,
                    lte: dateTo ? moment(dateTo).toDate() : undefined,
                },
            },
        });
        if (!trainings) {
            throw new common_1.NotFoundException('Nebyli nalezené žádne tréningy');
        }
        return trainings;
    }
    async getOne(id) {
        const training = await this.prisma.tRAININGS.findFirst({
            where: { TN_ID: parseInt(id) },
            include: {
                RESERVATION: true,
            },
        });
        if (!training) {
            throw new common_1.NotFoundException('Neexistuje tréning s daným ID.');
        }
        return training;
    }
    async createTraining(cycle, date, name, hour, maxMembers, maxSubs, description, color, bgColor) {
        if (!cycle || !date || !name || !hour || !maxMembers || !maxSubs) {
            throw new common_1.BadRequestException('Prosím vyplňte všetky potrebné políčka');
        }
        if (cycle === 1) {
            const training = await this.prisma.tRAININGS.create({
                data: {
                    TN_NAME: name,
                    TN_DATE: new Date(date),
                    TN_HOUR: hour,
                    TN_MAXMEM: maxMembers,
                    TN_MAXSUB: maxSubs,
                    TN_REMARK: description || undefined,
                    TN_COLOR: color || undefined,
                    TN_BGCOLOR: bgColor || undefined,
                },
            });
            if (!training) {
                throw new common_1.InternalServerErrorException('Nastala chyba pri vytvorení tréningu.');
            }
            return training;
        }
        else if (cycle > 1) {
            const allTrainings = [];
            for (let i = 0; i < cycle; i++) {
                const training = {
                    TN_NAME: name,
                    TN_DATE: new Date(new Date(date).setDate(new Date(date).getDate() + 7 * i)),
                    TN_HOUR: hour,
                    TN_MAXMEM: maxMembers,
                    TN_MAXSUB: maxSubs,
                    TN_REMARK: description || undefined,
                    TN_COLOR: color || undefined,
                    TN_BGCOLOR: bgColor || undefined,
                };
                allTrainings.push(training);
            }
            const trainings = await this.prisma.tRAININGS.createMany({
                data: allTrainings,
            });
            if (!trainings) {
                throw new common_1.InternalServerErrorException('Nastala chyba pri vytvorení tréningu.');
            }
            return trainings;
        }
    }
    async update(id, date, name, hour, maxMembers, maxSubs, description, color, bgColor) {
        if (!date &&
            !name &&
            !hour &&
            !maxMembers &&
            !maxSubs &&
            !description &&
            !color &&
            bgColor) {
            throw new common_1.BadRequestException('Prosím vyplňte potrebné údaje');
        }
        const editedTraining = await this.prisma.tRAININGS.update({
            where: {
                TN_ID: parseInt(id),
            },
            data: {
                TN_DATE: date ? new Date(date) : undefined,
                TN_NAME: name || undefined,
                TN_BGCOLOR: bgColor || undefined,
                TN_COLOR: color || undefined,
                TN_HOUR: hour || undefined,
                TN_MAXMEM: maxMembers || undefined,
                TN_MAXSUB: maxSubs || undefined,
                TN_REMARK: description || undefined,
            },
        });
        if (!editedTraining) {
            throw new common_1.NotFoundException('Neexistuje upravovaný tréning');
        }
        return editedTraining;
    }
    async delete(id) {
        const deletedPost = await this.prisma.tRAININGS.delete({
            where: {
                TN_ID: parseInt(id),
            },
        });
        if (!deletedPost) {
            throw new common_1.NotFoundException('Stala sa chyba pri mazání tréningu');
        }
        return deletedPost;
    }
};
TrainingService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], TrainingService);
exports.TrainingService = TrainingService;
//# sourceMappingURL=training.service.js.map