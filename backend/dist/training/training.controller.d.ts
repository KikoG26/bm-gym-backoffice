import { TrainingService } from './training.service';
import { Request } from 'express';
export declare class TrainingController {
    private readonly trainingService;
    constructor(trainingService: TrainingService);
    getAll(page: string, name: string, hour: string, dateFrom: string, dateTo: string, showAll: boolean): Promise<(import(".prisma/client").TRAININGS & {
        RESERVATION: import(".prisma/client").RESERVATION[];
        _count: {
            RESERVATION: number;
        };
    })[]>;
    getOne(id: string): Promise<import(".prisma/client").TRAININGS & {
        RESERVATION: import(".prisma/client").RESERVATION[];
    }>;
    create(request: Request): Promise<import(".prisma/client").TRAININGS | import(".prisma/client").Prisma.BatchPayload>;
    update(id: string, request: Request): Promise<import(".prisma/client").TRAININGS>;
    delete(id: string): Promise<import(".prisma/client").TRAININGS>;
}
