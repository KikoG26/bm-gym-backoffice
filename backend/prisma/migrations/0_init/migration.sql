-- CreateTable
CREATE TABLE `ACTUALITY` (
    `ACT_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `ACT_HEADING` VARCHAR(100) NOT NULL,
    `ACT_TEXT` TEXT NOT NULL,

    PRIMARY KEY (`ACT_ID`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ADMIN` (
    `ADM_LOGIN` VARCHAR(25) NOT NULL,
    `ADM_PASSWORD` VARCHAR(255) NOT NULL,

    PRIMARY KEY (`ADM_LOGIN`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `FORM` (
    `FRM_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `FRM_EMAIL` VARCHAR(75) NOT NULL,
    `FRM_NAME` VARCHAR(75) NULL,
    `FRM_MESSAGE` TEXT NOT NULL,
    `FRM_STATE` ENUM('NEW', 'OPENED', 'HIGHLIGHTED') NOT NULL,
    `FRM_DELETED` INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (`FRM_ID`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `RESERVATION` (
    `RES_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `RES_TN_ID` INTEGER NOT NULL,
    `RES_USR_EMAIL` VARCHAR(150) NOT NULL,
    `RES_PUBLIC_ID` VARCHAR(255) NOT NULL,
    `RES_FULLNAME` VARCHAR(150) NULL,
    `RES_REMOVED` INTEGER NOT NULL DEFAULT 0,
    `RES_IS_SUB` INTEGER NOT NULL DEFAULT 0,

    INDEX `RESERVATION_ibfk_1`(`RES_TN_ID`),
    INDEX `RESERVATION_ibfk_2`(`RES_USR_EMAIL`),
    PRIMARY KEY (`RES_ID`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `TRAININGS` (
    `TN_ID` INTEGER NOT NULL AUTO_INCREMENT,
    `TN_NAME` VARCHAR(100) NOT NULL,
    `TN_COLOR` VARCHAR(7) NULL,
    `TN_BGCOLOR` VARCHAR(7) NULL,
    `TN_DATE` DATE NOT NULL,
    `TN_HOUR` VARCHAR(5) NOT NULL,
    `TN_REMARK` TEXT NULL,
    `TN_MAXMEM` INTEGER NOT NULL,
    `TN_MAXSUB` INTEGER NOT NULL,

    PRIMARY KEY (`TN_ID`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `USERS` (
    `USR_EMAIL` VARCHAR(150) NOT NULL,
    `USR_PIN` INTEGER NOT NULL,

    PRIMARY KEY (`USR_EMAIL`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `WEBSITE_CMS` (
    `CMS_KEY` VARCHAR(50) NOT NULL,
    `CMS_VALUE` TEXT NOT NULL,

    PRIMARY KEY (`CMS_KEY`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `RESERVATION` ADD CONSTRAINT `RESERVATION_ibfk_1` FOREIGN KEY (`RES_TN_ID`) REFERENCES `TRAININGS`(`TN_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `RESERVATION` ADD CONSTRAINT `RESERVATION_ibfk_2` FOREIGN KEY (`RES_USR_EMAIL`) REFERENCES `USERS`(`USR_EMAIL`) ON DELETE CASCADE ON UPDATE CASCADE;

