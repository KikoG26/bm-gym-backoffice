import { createRouter, createWebHistory } from "vue-router";
import LoginView from "../views/LoginView.vue";
import DashboardView from "../views/DashboardView.vue";
import TrainingsView from "../views/TrainingsView.vue";
import MembersView from "../views/MembersView.vue";
import GalleryView from "../views/GalleryView.vue";
import NewsView from "../views/NewsView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      component: LoginView,
      meta: {
        title: "BM Gym - Přihlásení",
      },
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: DashboardView,
      meta: {
        title: "BM Gym - Dashboard",
      },
    },
    {
      path: "/treninky",
      name: "treninky",
      component: TrainingsView,
      meta: {
        title: "BM Gym - Tréninky",
      },
    },
    {
      path: "/clenove",
      name: "clenove",
      component: MembersView,
      meta: {
        title: "BM Gym - Členové",
      },
    },
    {
      path: "/galerie",
      name: "galerie",
      component: GalleryView,
      meta: {
        title: "BM Gym - Galerie",
      },
    },
    {
      path: "/aktuality",
      name: "aktuality",
      component: NewsView,
      meta: {
        title: "BM Gym - Aktuality",
      },
    },
  ],
});

router.beforeEach((to, from) => {
  document.title = to.meta?.title ?? "BM Gym - Admin";
});

export default router;
