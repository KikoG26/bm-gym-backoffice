import type { Training } from "@/views/TrainingsView.vue";

export const filterActiveReservations = (training: Training) => {
  const activeReservations = training.RESERVATION.filter(
    (tn) => tn.RES_REMOVED === 0
  );
  return activeReservations.length;
};
